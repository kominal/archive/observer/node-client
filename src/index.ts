import { AsyncClient, connect } from 'async-mqtt';
import { ENVIRONMENT_NAME, OBSERVER_MQTT_BROKER, OBSERVER_TENANT_ID, PROJECT_NAME, SERVICE_NAME, TASK_ID } from './helper/environment';

export * from './event';
export * from './log';

export let mqttClient: AsyncClient | undefined;

export function startObserver() {
	if (mqttClient || !OBSERVER_MQTT_BROKER) {
		return;
	}
	if (!OBSERVER_TENANT_ID || !PROJECT_NAME || !ENVIRONMENT_NAME || !SERVICE_NAME || !TASK_ID) {
		throw new Error('Missing required Observer parameter.');
	}

	mqttClient = connect(OBSERVER_MQTT_BROKER);
}
