import { printConsoleMessage } from '@kominal/lib-node-logging';
import { Level } from '@kominal/lib-node-logging/level';
import { mqttClient } from '.';
import { ENVIRONMENT_NAME, OBSERVER_TENANT_ID, PROJECT_NAME, SERVICE_NAME, TASK_ID } from './helper/environment';

export async function sendEvent(type: string, content: any) {
	if (!mqttClient) {
		return;
	}

	try {
		await mqttClient.publish(
			`${OBSERVER_TENANT_ID}/${PROJECT_NAME}/${ENVIRONMENT_NAME}/${SERVICE_NAME}/${TASK_ID}/event`,
			JSON.stringify({
				time: new Date(),
				type,
				content,
			}),
			{ qos: 0 }
		);
	} catch (e) {
		printConsoleMessage(Level.ERROR, `Could not forward event to Observer: ${e}`);
	}
}
