import { printConsoleMessage } from '@kominal/lib-node-logging';
import { Level } from '@kominal/lib-node-logging/level';
import { mqttClient } from '.';
import { ENVIRONMENT_NAME, OBSERVER_TENANT_ID, PROJECT_NAME, SERVICE_NAME, TASK_ID } from './helper/environment';

export async function log(level: Level, message: any): Promise<void> {
	printConsoleMessage(level, message instanceof Error ? message.message : message);

	if (!mqttClient) {
		return;
	}

	try {
		await mqttClient.publish(
			`${OBSERVER_TENANT_ID}/${PROJECT_NAME}/${ENVIRONMENT_NAME}/${SERVICE_NAME}/${TASK_ID}/log`,
			JSON.stringify({
				time: new Date(),
				level,
				message,
			}),
			{ qos: 0 }
		);
	} catch (e) {
		printConsoleMessage(Level.ERROR, `Could not forward event to Observer: ${e}`);
	}
}

export async function debug(message: any): Promise<void> {
	return log(Level.DEBUG, message);
}

export async function info(message: any): Promise<void> {
	return log(Level.INFO, message);
}

export async function warn(message: any): Promise<void> {
	return log(Level.WARN, message);
}

export async function error(message: any): Promise<void> {
	return log(Level.ERROR, message);
}
