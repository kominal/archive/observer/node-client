export const OBSERVER_MQTT_BROKER = process.env.OBSERVER_MQTT_BROKER;
export const OBSERVER_TENANT_ID = process.env.OBSERVER_TENANT_ID;
export const PROJECT_NAME = process.env.PROJECT_NAME;
export const ENVIRONMENT_NAME = process.env.ENVIRONMENT_NAME;
export const SERVICE_NAME = process.env.SERVICE_NAME;
export const TASK_ID = process.env.TASK_ID;
